﻿using System;
using LexicographicSearch;
using LexicographicSearch.Objectives;
using LexicographicSearchStateMachine;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.Patient;
using RadiationTreatmentPlanner.Utils.Volume;

namespace ExampleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var ptvDomStructureId = "PtvDom";
            var gtvStructureId = "GTV";
            var oarStructureId = "OAR";

            var strategy = CreateStateMachineStrategy();
            var machine = new LSStateMachine(strategy, gtvStructureId, ptvDomStructureId, oarStructureId);
            
        }

        private static void RunOneRound(LSStateMachine machine)
        {
        }

        static IStateMachineStrategy CreateStateMachineStrategy()
        {
            return new MyStrategy();
        }
    }

    class MyStrategy : IStateMachineStrategy
    {
        public LSPlanSetup CreateInitialPlanSetup(string ptvDomStructureId, string gtvStructureId, string oarStructureId)
        {
            var pointObjectives = new PointObjective[]
            {
                VmatObjectivesFactory.BuildPointObjectiveAsSoftConstraint(gtvStructureId,
                    UDoseStructureConstraint.ObjectiveOperator.LOWER, Priority.Factory.BuildPriority(300),
                    new UDose(66, UDose.UDoseUnit.Gy), new UVolume(100, UVolume.VolumeUnit.percent)),
                VmatObjectivesFactory.BuildPointObjectiveAsSoftConstraint(ptvDomStructureId,
                    UDoseStructureConstraint.ObjectiveOperator.UPPER, Priority.Factory.BuildPriority(540),
                    new UDose(78, UDose.UDoseUnit.Gy), new UVolume(0, UVolume.VolumeUnit.percent)),
                VmatObjectivesFactory.BuildPointObjectiveAsSoftConstraint(ptvDomStructureId,
                    UDoseStructureConstraint.ObjectiveOperator.LOWER, Priority.Factory.BuildPriority(540),
                    new UDose(69, UDose.UDoseUnit.Gy), new UVolume(96, UVolume.VolumeUnit.percent)),
                VmatObjectivesFactory.BuildPointObjectiveAsSoftConstraint(oarStructureId,
                    UDoseStructureConstraint.ObjectiveOperator.UPPER, Priority.Factory.BuildPriority(600),
                    new UDose(45, UDose.UDoseUnit.Gy), new UVolume(96, UVolume.VolumeUnit.percent)),
            };

            var eudObjectives = new EUDObjective[]
            {
                VmatObjectivesFactory.BuildEudObjectiveAsSoftConstraint(ptvDomStructureId,
                    UDoseStructureConstraint.ObjectiveOperator.LOWER, Priority.Factory.BuildPriority(480),
                    new UDose(66, UDose.UDoseUnit.Gy), -40),
                VmatObjectivesFactory.BuildEudObjectiveAsSoftConstraint(oarStructureId,
                    UDoseStructureConstraint.ObjectiveOperator.UPPER, Priority.Factory.BuildPriority(360),
                    new UDose(40, UDose.UDoseUnit.Gy), 40),
                VmatObjectivesFactory.BuildEudObjectiveAsHardConstraint("Kidney",
                    UDoseStructureConstraint.ObjectiveOperator.UPPER, Priority.Factory.BuildPriority(300),
                    new UDose(20, UDose.UDoseUnit.Gy), 40),
            };

            var meanDoseObjectives = new MeanDoseObjective[]
            {
                VmatObjectivesFactory.BuildMeanDoseObjectiveAsHardConstraint("Kidney",
                    new UDose(16, UDose.UDoseUnit.Gy), Priority.Factory.BuildPriority(480)),
            };

            return new LSPlanSetup("Initial Plan", pointObjectives, eudObjectives, meanDoseObjectives);
        }

        public object OptimizeCurrentPlanSetup(LSPlanSetup planSetup)
        {
            return "Placeholder result";
        }

        public LSPlanSetupEvaluationResult EvaluateCurrentPlanSetup(LSPlanSetup planSetup,
            object currentOptimizationResult)
        {
            return new LSPlanSetupEvaluationResult(planSetup, true);
        }
    }
}
