﻿using System;

namespace LexicographicSearch
{
    public class LSPlanSetupEvaluationResult
    {
        public LSPlanSetup PlanSetup { get; }
        public bool Success { get; }
        public object AdditionalParameter { get; }

        public LSPlanSetupEvaluationResult(LSPlanSetup planSetup, bool success, object additionalParameter = null)
        {
            PlanSetup = planSetup ?? throw new ArgumentNullException(nameof(planSetup));
            Success = success;
            AdditionalParameter = additionalParameter;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as LSPlanSetupEvaluationResult);
        }
        public bool Equals(LSPlanSetupEvaluationResult other)
        {
            return other != null
                   && Equals(PlanSetup, other.PlanSetup)
                   && Success == other.Success
                   && Equals(AdditionalParameter, other.AdditionalParameter);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (PlanSetup != null ? PlanSetup.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Success.GetHashCode();
                hashCode = (hashCode * 397) ^ (AdditionalParameter != null ? AdditionalParameter.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}
