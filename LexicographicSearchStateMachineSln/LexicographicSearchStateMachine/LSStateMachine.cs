﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using LexicographicSearch;
using LiquidState;
using LiquidState.Core;
using LiquidState.Synchronous.Core;
using RadiationTreatmentPlanner.Utils.OptimizationObjetives;

namespace LexicographicSearchStateMachine
{
    public partial class LSStateMachine
    {
        private readonly IStateMachineStrategy _stateMachineStrategy;
        private List<LSPlanSetupEvaluationResult> _evaluationResultHistory;
        private List<object> _optimizationResultHistory;
        private readonly IStateMachine<LexicographicSearchState, LexicographicSearchTrigger> _stateMachine;
        private readonly ParameterizedTrigger<LexicographicSearchTrigger, LSPlanSetup> _startTriggerWithParameters;
        private readonly ParameterizedTrigger<LexicographicSearchTrigger, PlanCreationConfig> _createNextPlanTriggerWithParameters;

        public ReadOnlyCollection<LSPlanSetupEvaluationResult> EvaluationResultHistory => _evaluationResultHistory.AsReadOnly();
        public ReadOnlyCollection<object> OptimizationResultHistory => _optimizationResultHistory.AsReadOnly();

        public LexicographicSearchState CurrentState => _stateMachine.CurrentState;
        public IStateMachineDiagnostics<LexicographicSearchState, LexicographicSearchTrigger> Diagnostics => _stateMachine.Diagnostics;
        public LSPlanSetup LastCreatedPlanSetup { get; private set; }
        public object LastOptimizationResult => _optimizationResultHistory.LastOrDefault();
        public LSPlanSetupEvaluationResult LastEvaluatedPlanSetup => _evaluationResultHistory.LastOrDefault();

        public string GtvStructureId { get; }
        public string PtvDomStructureId { get; }
        public string OarStructureId { get; }
        public LSPlanSetup InitialPlanSetup { get; private set; }


        public enum LexicographicSearchState
        {
            InitialState,
            SweepPriorities,
            Optimize,
            Evaluate,
            Finished
        }

        public enum LexicographicSearchTrigger
        {
            Start,
            CreateNextPlan,
            OptimizePlanSetup,
            EvaluatePlanSetup,
            Finish
        }

        public LSStateMachine(IStateMachineStrategy stateMachineStrategy)
        {
            _stateMachineStrategy = stateMachineStrategy ?? throw new ArgumentNullException(nameof(stateMachineStrategy));

            _evaluationResultHistory = new List<LSPlanSetupEvaluationResult>();
            _optimizationResultHistory = new List<object>();

            var stateMachineConfig = StateMachineFactory
                .CreateConfiguration<LexicographicSearchState, LexicographicSearchTrigger>();

            _startTriggerWithParameters = stateMachineConfig.SetTriggerParameter<LSPlanSetup>(LexicographicSearchTrigger.Start);
            stateMachineConfig.ForState(LexicographicSearchState.InitialState)
                .Permit(_startTriggerWithParameters, LexicographicSearchState.Optimize, OnSetInitialPlanSetup)
                .Permit(LexicographicSearchTrigger.Finish, LexicographicSearchState.Finished);

            stateMachineConfig.ForState(LexicographicSearchState.Optimize)
                .Permit(LexicographicSearchTrigger.OptimizePlanSetup, LexicographicSearchState.Evaluate, OnOptimizePlanSetup)
                .Permit(LexicographicSearchTrigger.Finish, LexicographicSearchState.Finished);

            stateMachineConfig.ForState(LexicographicSearchState.Evaluate)
                .Permit(LexicographicSearchTrigger.EvaluatePlanSetup, LexicographicSearchState.SweepPriorities,
                    OnEvaluatePlanSetup)
                .Permit(LexicographicSearchTrigger.Finish, LexicographicSearchState.Finished);

            _createNextPlanTriggerWithParameters =
                stateMachineConfig.SetTriggerParameter<PlanCreationConfig>(LexicographicSearchTrigger.CreateNextPlan);
            stateMachineConfig.ForState(LexicographicSearchState.SweepPriorities)
                .Permit(_createNextPlanTriggerWithParameters, LexicographicSearchState.Optimize, OnCreateNextPlan)
                .Permit(LexicographicSearchTrigger.Finish, LexicographicSearchState.Finished);

            stateMachineConfig.ForState(LexicographicSearchState.Finished);

            _stateMachine = StateMachineFactory.Create(LexicographicSearchState.InitialState, stateMachineConfig);
        }

        private void OnSetInitialPlanSetup(LSPlanSetup plan)
        {
            InitialPlanSetup = plan;
            LastCreatedPlanSetup = plan;
        }

        /*
         * Private trigger methods.
         */
        private void OnCreateNextPlan(PlanCreationConfig planCreationConfig)
        {
            var structureIds = planCreationConfig.StructureIds;
            var tag = planCreationConfig.Tag;
            var objectives = MaximizePrioritiesOfObjectivesForStructures(structureIds, InitialPlanSetup.Objectives);
            LastCreatedPlanSetup = new LSPlanSetup(tag, objectives.ToArray());
        }

        private IEnumerable<IObjective> MaximizePrioritiesOfObjectivesForStructures(
            IEnumerable<string> structureIds, IEnumerable<IObjective> objectives)
        {
            if (structureIds == null) throw new ArgumentNullException(nameof(structureIds));
            if (objectives == null) throw new ArgumentNullException(nameof(objectives));

            var resultObjectives = new List<IObjective>();
            foreach (var objective in objectives)
            {
                if (!structureIds.Contains(objective.StructureId))
                {
                    resultObjectives.Add(objective);
                    continue;
                }

                switch (objective)
                {
                    case PointObjective pointObjective:
                        resultObjectives.Add(MaximizePriorityForObjective(pointObjective));
                        break;
                    case EUDObjective eudObjective:
                        resultObjectives.Add(MaximizePriorityForObjective(eudObjective));
                        break;
                    case MeanDoseObjective meanDoseObjective:
                        resultObjectives.Add(MaximizePriorityForObjective(meanDoseObjective));
                        break;
                }
            }

            return resultObjectives;
        }

        private IObjective MaximizePriorityForObjective(PointObjective objective)
        {
            var maximumPriority = Priority.Factory.BuildMaximumPriority();
            return ObjectivesFactory.BuildPointObjective(objective.StructureId,
                objective.ObjectiveOperator, maximumPriority, objective.Dose, objective.VolumeInPercent);
        }

        private IObjective MaximizePriorityForObjective(EUDObjective objective)
        {
            var maximumPriority = Priority.Factory.BuildMaximumPriority();
            return ObjectivesFactory.BuildEudObjective(objective.StructureId,
                objective.ObjectiveOperator, maximumPriority, objective.Dose, objective.ParameterA);
        }

        private IObjective MaximizePriorityForObjective(MeanDoseObjective objective)
        {
            var maximumPriority = Priority.Factory.BuildMaximumPriority();
            return ObjectivesFactory.BuildMeanDoseObjective(objective.StructureId, objective.Dose,
                maximumPriority);
        }

        private void OnOptimizePlanSetup()
        {
            var optimizerResult = _stateMachineStrategy.OptimizeCurrentPlanSetup(LastCreatedPlanSetup);
            _optimizationResultHistory.Add(optimizerResult);
        }
        private void OnEvaluatePlanSetup()
        {
            var evaluationResult = _stateMachineStrategy.EvaluateCurrentPlanSetup(LastCreatedPlanSetup, LastOptimizationResult);
            _evaluationResultHistory.Add(evaluationResult);
        }


        /*
         * Private methods.
         */
        private List<MeanDoseObjective> MaximizePrioritiesForMeanDoseObjectivesOfStructures(
            IEnumerable<string> structureIds, IEnumerable<MeanDoseObjective> previousObjectives)
        {
            var meanDoseObjectives = new List<MeanDoseObjective>();
            foreach (var objective in previousObjectives)
            {
                if (structureIds.Contains(objective.StructureId))
                    meanDoseObjectives.Add(new MeanDoseObjective(objective.StructureId, objective.Dose,
                        Priority.Factory.BuildMaximumPriority()));
                else
                    meanDoseObjectives.Add(objective);
            }

            return meanDoseObjectives;
        }

        private List<EUDObjective> MaximizePrioritiesForEUDObjectivesOfStructures(
            IEnumerable<string> structureIds, IEnumerable<EUDObjective> previousObjectives)
        {
            var eudObjectives = new List<EUDObjective>();
            foreach (var objective in previousObjectives)
            {
                if (structureIds.Contains(objective.StructureId))
                    eudObjectives.Add(new EUDObjective(objective.StructureId, objective.ObjectiveOperator,
                        Priority.Factory.BuildMaximumPriority(), objective.Dose, objective.ParameterA));
                else
                    eudObjectives.Add(objective);
            }

            return eudObjectives;
        }

        private List<PointObjective> MaximizePrioritiesForPointObjectivesOfStructures(
            IEnumerable<string> structureIds, IEnumerable<PointObjective> previousObjectives)
        {
            var pointObjectives = new List<PointObjective>();
            foreach (var objective in previousObjectives)
            {
                if (structureIds.Contains(objective.StructureId))
                    pointObjectives.Add(new PointObjective(objective.StructureId, objective.ObjectiveOperator,
                        Priority.Factory.BuildMaximumPriority(), objective.Dose, objective.VolumeInPercent));
                else
                    pointObjectives.Add(objective);
            }

            return pointObjectives;
        }

        /*
         * Public trigger methods.
         */
        public void Start(LSPlanSetup planSetup)
        {
            if (planSetup == null) throw new ArgumentNullException(nameof(planSetup));
            _stateMachine.Fire(_startTriggerWithParameters, planSetup);
        }

        /// <summary>
        /// Create the next plan setup.
        /// </summary>
        public void CreateNextPlanSetup(PlanCreationConfig planCreationConfig)
        {
            if (planCreationConfig == null) throw new ArgumentNullException(nameof(planCreationConfig));
            _stateMachine.Fire(_createNextPlanTriggerWithParameters, planCreationConfig);
        }

        /// <summary>
        /// Optimize the last plan setup that was created.
        /// </summary>
        /// <returns></returns>
        public void OptimizeCurrentPlanSetup() => _stateMachine.Fire(LexicographicSearchTrigger.OptimizePlanSetup);

        /// <summary>
        /// Evaluate the last plan setup that was created.
        /// </summary>
        public void EvaluateCurrentPlanSetup() => _stateMachine.Fire(LexicographicSearchTrigger.EvaluatePlanSetup);

        /// <summary>
        /// Finish state machine.
        /// </summary>
        public void Finish() => _stateMachine.Fire(LexicographicSearchTrigger.Finish);
    }
}
