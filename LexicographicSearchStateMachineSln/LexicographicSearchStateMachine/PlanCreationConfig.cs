﻿using System;
using System.Collections.Generic;

namespace LexicographicSearch
{
    public class PlanCreationConfig
    {
        public IEnumerable<string> StructureIds { get; }
        public object Tag { get; }

        public PlanCreationConfig(IEnumerable<string> structureIds, object tag)
        {
            StructureIds = structureIds ?? throw new ArgumentNullException(nameof(structureIds));
            Tag = tag ?? throw new ArgumentNullException(nameof(tag));
        }

        protected bool Equals(PlanCreationConfig other)
        {
            return Equals(StructureIds, other.StructureIds) && Equals(Tag, other.Tag);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((PlanCreationConfig)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(StructureIds, Tag);
        }
    }
}
