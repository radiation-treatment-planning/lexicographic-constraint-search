﻿namespace LexicographicSearch
{
    public interface IStateMachineStrategy
    {
        object OptimizeCurrentPlanSetup(LSPlanSetup planSetup);
        LSPlanSetupEvaluationResult EvaluateCurrentPlanSetup(LSPlanSetup planSetup, object currentOptimizationResult);
    }
}
