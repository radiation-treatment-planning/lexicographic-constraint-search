﻿using System;
using System.Collections.Generic;
using System.Linq;
using RadiationTreatmentPlanner.Utils.OptimizationObjetives;

namespace LexicographicSearch
{
    public class LSPlanSetup
    {

        private readonly IObjective[] _objectives;

        public IEnumerable<IObjective> Objectives => _objectives.AsEnumerable();

        public object Tag { get; }

        public LSPlanSetup(object tag, IObjective[] objectives)
        {
            _objectives = objectives ?? throw new ArgumentNullException(nameof(objectives));
            Tag = tag ?? throw new ArgumentNullException(nameof(tag));
        }

        public override bool Equals(object other)
        {
            return Equals(other as LSPlanSetup);
        }

        protected bool Equals(LSPlanSetup other)
        {
            if (other == null) return false;
            if (_objectives.Length != other._objectives.Length) return false;
            foreach (var objective in _objectives)
                if (!_objectives.Contains(objective))
                    return false;

            return Equals(Tag, other.Tag);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                return ((_objectives != null ? _objectives.GetHashCode() : 0) * 397) ^ (Tag != null ? Tag.GetHashCode() : 0);
            }
        }
    }
}
