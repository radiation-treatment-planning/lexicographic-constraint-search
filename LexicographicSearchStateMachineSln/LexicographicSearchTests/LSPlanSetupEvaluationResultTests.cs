﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LexicographicSearch;
using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.OptimizationObjetives;
using RadiationTreatmentPlanner.Utils.Volume;

namespace LexicographicSearchTests
{
    [TestFixture]
    public class LSPlanSetupEvaluationResultTests
    {
        [Test]
        public void Equals_Test()
        {

            var objectives = new List<IObjective>();
            objectives.Add(ObjectivesFactory.BuildEudObjective("s1",
                ObjectiveOperator.LOWER, new Priority(10), new UDose(66, UDose.UDoseUnit.Gy),
                10));
            objectives.Add(ObjectivesFactory.BuildMeanDoseObjective("s2",
                new UDose(40, UDose.UDoseUnit.Gy), new Priority(100)));
            objectives.Add(ObjectivesFactory.BuildPointObjective("s3",
                ObjectiveOperator.EXACT, new Priority(200), new UDose(50, UDose.UDoseUnit.Gy),
                new UVolume(100, UVolume.VolumeUnit.percent)));
            objectives.Add(ObjectivesFactory.BuildPointObjective("s3",
                ObjectiveOperator.UPPER, new Priority(300), new UDose(60, UDose.UDoseUnit.Gy),
                new UVolume(100, UVolume.VolumeUnit.percent)));

            var planSetup = new LSPlanSetup("A random tag", objectives.ToArray());
            var evaluationResult = new LSPlanSetupEvaluationResult(planSetup, true, "Whatever");
            var evaluationResult2 = new LSPlanSetupEvaluationResult(planSetup, true, "Whatever");

            Assert.AreEqual(evaluationResult, evaluationResult2);
        }

        [Test]
        public void Equals_ReturnFalseIfAdditionalParametersAreNotEqual_Test()
        {

            var objectives = new List<IObjective>();
            objectives.Add(ObjectivesFactory.BuildEudObjective("s1",
                ObjectiveOperator.LOWER, new Priority(10), new UDose(66, UDose.UDoseUnit.Gy),
                10));
            objectives.Add(ObjectivesFactory.BuildMeanDoseObjective("s2",
                new UDose(40, UDose.UDoseUnit.Gy), new Priority(100)));
            objectives.Add(ObjectivesFactory.BuildPointObjective("s3",
                ObjectiveOperator.EXACT, new Priority(200), new UDose(50, UDose.UDoseUnit.Gy),
                new UVolume(100, UVolume.VolumeUnit.percent)));
            objectives.Add(ObjectivesFactory.BuildPointObjective("s3",
                ObjectiveOperator.UPPER, new Priority(300), new UDose(60, UDose.UDoseUnit.Gy),
                new UVolume(100, UVolume.VolumeUnit.percent)));

            var planSetup = new LSPlanSetup("A random tag", objectives.ToArray());
            var evaluationResult = new LSPlanSetupEvaluationResult(planSetup, true, "Whatever");
            var evaluationResult2 = new LSPlanSetupEvaluationResult(planSetup, true, "Something else");

            Assert.AreNotEqual(evaluationResult, evaluationResult2);
        }

        [Test]
        public void Equals_ReturnFalseIfPlanSetupsAreNotEqual_Test()
        {

            var objectives = new List<IObjective>();
            objectives.Add(ObjectivesFactory.BuildEudObjective("s1",
                ObjectiveOperator.LOWER, new Priority(10), new UDose(66, UDose.UDoseUnit.Gy),
                10));
            objectives.Add(ObjectivesFactory.BuildMeanDoseObjective("s2",
                new UDose(40, UDose.UDoseUnit.Gy), new Priority(100)));
            objectives.Add(ObjectivesFactory.BuildPointObjective("s3",
                ObjectiveOperator.EXACT, new Priority(200), new UDose(50, UDose.UDoseUnit.Gy),
                new UVolume(100, UVolume.VolumeUnit.percent)));
            objectives.Add(ObjectivesFactory.BuildPointObjective("s3",
                ObjectiveOperator.UPPER, new Priority(300), new UDose(60, UDose.UDoseUnit.Gy),
                new UVolume(100, UVolume.VolumeUnit.percent)));

            var objectives2 = objectives.ToList();
            objectives2.RemoveAt(0);


            var planSetup = new LSPlanSetup("A random tag", objectives.ToArray());
            var planSetup2 = new LSPlanSetup("A random tag", objectives2.ToArray());
            var evaluationResult = new LSPlanSetupEvaluationResult(planSetup, true, "Whatever");
            var evaluationResult2 = new LSPlanSetupEvaluationResult(planSetup2, true, "Whatever");

            Assert.AreNotEqual(evaluationResult, evaluationResult2);
        }
    }
}
