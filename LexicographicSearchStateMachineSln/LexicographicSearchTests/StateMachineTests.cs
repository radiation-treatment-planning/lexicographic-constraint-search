﻿using System.Collections.Generic;
using LexicographicSearch;
using LexicographicSearchStateMachine;
using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.OptimizationObjetives;
using RadiationTreatmentPlanner.Utils.Volume;

namespace LexicographicSearchTests
{
    [TestFixture]
    public class StateMachineTests
    {
        private string _ptvDomStructureId;
        private string _gtvStructureId;
        private string _oarStructureId;
        private LSPlanSetup _initialPlanSetup;

        [SetUp]
        public void SetUp()
        {
            _ptvDomStructureId = "PtvDom";
            _gtvStructureId = "GTV";
            _oarStructureId = "OAR";

            _initialPlanSetup = CreateInitialPlanSetup();
        }

        [Test]
        public void Initialization_Test()
        {
            var strategy = new MyStrategy();
            var machine = new LSStateMachine(strategy);
        }


        [Test]
        public void RunCompleteProcedure_Test()
        {
            var strategy = new MyStrategy();
            var machine = new LSStateMachine(strategy);

            Assert.AreEqual(LSStateMachine.LexicographicSearchState.InitialState, machine.CurrentState);

            machine.Start(_initialPlanSetup);
            Assert.AreEqual(LSStateMachine.LexicographicSearchState.Optimize, machine.CurrentState);

            machine.OptimizeCurrentPlanSetup();
            Assert.AreEqual(LSStateMachine.LexicographicSearchState.Evaluate, machine.CurrentState);

            machine.EvaluateCurrentPlanSetup();
            Assert.AreEqual(LSStateMachine.LexicographicSearchState.SweepPriorities, machine.CurrentState);

            machine.CreateNextPlanSetup(new PlanCreationConfig(new List<string> { _ptvDomStructureId }, "Placeholder tag"));
            Assert.AreEqual(LSStateMachine.LexicographicSearchState.Optimize, machine.CurrentState);

            machine.OptimizeCurrentPlanSetup();
            Assert.AreEqual(LSStateMachine.LexicographicSearchState.Evaluate, machine.CurrentState);

            machine.EvaluateCurrentPlanSetup();
            Assert.AreEqual(LSStateMachine.LexicographicSearchState.SweepPriorities, machine.CurrentState);

            machine.Finish();
            Assert.AreEqual(LSStateMachine.LexicographicSearchState.Finished, machine.CurrentState);

        }

        [Test]
        public void StartMachineWithInitialPlanSetup_Test()
        {
            var strategy = new MyStrategy();
            var machine = new LSStateMachine(strategy);

            machine.Start(_initialPlanSetup);
            Assert.AreEqual(LSStateMachine.LexicographicSearchState.Optimize, machine.CurrentState);
            Assert.AreEqual(_initialPlanSetup, machine.LastCreatedPlanSetup);
            Assert.IsEmpty(machine.OptimizationResultHistory);
            Assert.IsEmpty(machine.EvaluationResultHistory);
        }

        [Test]
        public void OptimizeCurrentPlanSetup_Test()
        {
            var strategy = new MyStrategy();
            var machine = new LSStateMachine(strategy);

            machine.Start(_initialPlanSetup);
            machine.OptimizeCurrentPlanSetup();

            var result = machine.LastOptimizationResult as OptimizationResult;
            Assert.AreEqual(true, result?.Success);
            Assert.AreEqual(1, machine.OptimizationResultHistory.Count);
            Assert.IsEmpty(machine.EvaluationResultHistory);
        }

        [Test]
        public void EvaluateCurrentPlanSetup_Test()
        {
            var strategy = new MyStrategy();
            var machine = new LSStateMachine(strategy);

            machine.Start(_initialPlanSetup);
            machine.OptimizeCurrentPlanSetup();
            machine.EvaluateCurrentPlanSetup();

            var result = machine.LastEvaluatedPlanSetup;
            Assert.IsNotEmpty(machine.EvaluationResultHistory);
            Assert.AreEqual(_initialPlanSetup, result.PlanSetup);
            Assert.AreEqual(false, result.Success);
        }

        [Test]
        public void CreateNextPlanSetup_Test()
        {
            var strategy = new MyStrategy();
            var machine = new LSStateMachine(strategy);

            machine.Start(_initialPlanSetup);
            machine.OptimizeCurrentPlanSetup();
            machine.EvaluateCurrentPlanSetup();
            var planCreationConfig =
                new PlanCreationConfig(new List<string> { _ptvDomStructureId, _gtvStructureId }, "A new plan");
            machine.CreateNextPlanSetup(planCreationConfig);

            var maximumPriority = Priority.Factory.BuildMaximumPriority();
            var objectives = machine.LastCreatedPlanSetup.Objectives;

            foreach (var objective in objectives)
            {
                if (objective.StructureId == _ptvDomStructureId || objective.StructureId == _gtvStructureId)
                    Assert.AreEqual(maximumPriority, objective.Priority);
                else
                    Assert.AreNotEqual(maximumPriority, objective.Priority);
            }
        }


        private LSPlanSetup CreateInitialPlanSetup()
        {
            var objectives = new IObjective[]
            {
                ObjectivesFactory.BuildPointObjective(_gtvStructureId,
                    ObjectiveOperator.LOWER, Priority.Factory.BuildPriority(300),
                    new UDose(66, UDose.UDoseUnit.Gy), new UVolume(100, UVolume.VolumeUnit.percent)),
                ObjectivesFactory.BuildPointObjective(_ptvDomStructureId,
                    ObjectiveOperator.UPPER, Priority.Factory.BuildPriority(540),
                    new UDose(78, UDose.UDoseUnit.Gy), new UVolume(0, UVolume.VolumeUnit.percent)),
                ObjectivesFactory.BuildPointObjective(_ptvDomStructureId,
                    ObjectiveOperator.LOWER, Priority.Factory.BuildPriority(540),
                    new UDose(69, UDose.UDoseUnit.Gy), new UVolume(96, UVolume.VolumeUnit.percent)),
                ObjectivesFactory.BuildPointObjective(_oarStructureId,
                    ObjectiveOperator.UPPER, Priority.Factory.BuildPriority(600),
                    new UDose(45, UDose.UDoseUnit.Gy), new UVolume(96, UVolume.VolumeUnit.percent)),
                ObjectivesFactory.BuildEudObjective(_ptvDomStructureId,
                    ObjectiveOperator.LOWER, Priority.Factory.BuildPriority(480),
                    new UDose(66, UDose.UDoseUnit.Gy), -40),
                ObjectivesFactory.BuildEudObjective(_oarStructureId,
                    ObjectiveOperator.UPPER, Priority.Factory.BuildPriority(360),
                    new UDose(40, UDose.UDoseUnit.Gy), 40),
                ObjectivesFactory.BuildEudObjective("Kidney",
                    ObjectiveOperator.UPPER, Priority.Factory.BuildPriority(300),
                    new UDose(20, UDose.UDoseUnit.Gy), 40),
                ObjectivesFactory.BuildMeanDoseObjective("Kidney",
                    new UDose(16, UDose.UDoseUnit.Gy), Priority.Factory.BuildPriority(480)),
            };

            var eudObjectives = new EUDObjective[]
            {

            };

            var meanDoseObjectives = new MeanDoseObjective[]
            {

            };

            return new LSPlanSetup("Initial Plan", objectives);
        }
    }

    class MyStrategy : IStateMachineStrategy
    {

        public object OptimizeCurrentPlanSetup(LSPlanSetup planSetup)
        {
            return new OptimizationResult(true);
        }

        public LSPlanSetupEvaluationResult EvaluateCurrentPlanSetup(LSPlanSetup planSetup,
            object currentOptimizationResult)
        {
            return new LSPlanSetupEvaluationResult(planSetup, false);
        }
    }

    class OptimizationResult
    {
        public bool Success { get; }

        public OptimizationResult(bool success)
        {
            Success = success;
        }
    }
}
