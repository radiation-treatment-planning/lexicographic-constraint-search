﻿using System.Collections.Generic;
using LexicographicSearch;
using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.OptimizationObjetives;
using RadiationTreatmentPlanner.Utils.Volume;

namespace LexicographicSearchTests
{
    [TestFixture]
    public class LSPlanSetupTests
    {
        [Test]
        public void Constructor_Test()
        {
            var objectives = new List<IObjective>();
            objectives.Add(ObjectivesFactory.BuildEudObjective("s1",
                ObjectiveOperator.LOWER, new Priority(10), new UDose(66, UDose.UDoseUnit.Gy),
                10));
            objectives.Add(ObjectivesFactory.BuildMeanDoseObjective("s2",
                new UDose(40, UDose.UDoseUnit.Gy), new Priority(100)));
            objectives.Add(ObjectivesFactory.BuildPointObjective("s3",
                ObjectiveOperator.EXACT, new Priority(200), new UDose(50, UDose.UDoseUnit.Gy),
                new UVolume(100, UVolume.VolumeUnit.percent)));
            objectives.Add(ObjectivesFactory.BuildPointObjective("s3",
                ObjectiveOperator.UPPER, new Priority(300), new UDose(60, UDose.UDoseUnit.Gy),
                new UVolume(100, UVolume.VolumeUnit.percent)));

            var planSetup = new LSPlanSetup("A random tag", objectives.ToArray());
            var numberOfPointObjectives = 0;
            var numberOfEudObjectives = 0;
            var numberOfMeanDoseObjectives = 0;

            foreach (var objective in planSetup.Objectives)
            {

                if (objective is PointObjective) numberOfPointObjectives += 1;
                if (objective is MeanDoseObjective) numberOfMeanDoseObjectives += 1;
                if (objective is EUDObjective) numberOfEudObjectives += 1;
            }

            Assert.AreEqual(1, numberOfEudObjectives);
            Assert.AreEqual(1, numberOfMeanDoseObjectives);
            Assert.AreEqual(2, numberOfPointObjectives);
        }

        [Test]
        public void Equals_Test()
        {
            var objectives = new List<IObjective>();
            objectives.Add(ObjectivesFactory.BuildEudObjective("s1",
                ObjectiveOperator.LOWER, new Priority(10), new UDose(66, UDose.UDoseUnit.Gy),
                10));
            objectives.Add(ObjectivesFactory.BuildMeanDoseObjective("s2",
                new UDose(40, UDose.UDoseUnit.Gy), new Priority(100)));
            objectives.Add(ObjectivesFactory.BuildPointObjective("s3",
                ObjectiveOperator.EXACT, new Priority(200), new UDose(50, UDose.UDoseUnit.Gy),
                new UVolume(100, UVolume.VolumeUnit.percent)));
            objectives.Add(ObjectivesFactory.BuildPointObjective("s3",
                ObjectiveOperator.UPPER, new Priority(300), new UDose(60, UDose.UDoseUnit.Gy),
                new UVolume(100, UVolume.VolumeUnit.percent)));

            var planSetup = new LSPlanSetup("A random tag", objectives.ToArray());
            var planSetup2 = new LSPlanSetup("A random tag", objectives.ToArray());

            Assert.AreEqual(planSetup2, planSetup);
        }
    }
}
